﻿namespace schuelerfoto
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cbxKlasse = new System.Windows.Forms.ComboBox();
            this.tsFooter = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblAttention = new System.Windows.Forms.Label();
            this.lblDragPics = new System.Windows.Forms.Label();
            this.btnPasswort = new System.Windows.Forms.Button();
            this.tbxPasswort = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsFoot = new System.Windows.Forms.ToolStripStatusLabel();
            this.pbxInfo = new System.Windows.Forms.PictureBox();
            this.pbxRect = new System.Windows.Forms.PictureBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRect)).BeginInit();
            this.SuspendLayout();
            // 
            // cbxKlasse
            // 
            this.cbxKlasse.BackColor = System.Drawing.Color.Blue;
            this.cbxKlasse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxKlasse.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbxKlasse.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxKlasse.ForeColor = System.Drawing.SystemColors.Window;
            this.cbxKlasse.FormattingEnabled = true;
            this.cbxKlasse.Location = new System.Drawing.Point(12, 15);
            this.cbxKlasse.Name = "cbxKlasse";
            this.cbxKlasse.Size = new System.Drawing.Size(241, 30);
            this.cbxKlasse.TabIndex = 0;
            this.cbxKlasse.SelectedIndexChanged += new System.EventHandler(this.CbxKlasse_SelectedIndexChanged);
            this.cbxKlasse.Click += new System.EventHandler(this.cbxKlasse_Click);
            // 
            // tsFooter
            // 
            this.tsFooter.Name = "tsFooter";
            this.tsFooter.Size = new System.Drawing.Size(23, 23);
            // 
            // lblAttention
            // 
            this.lblAttention.BackColor = System.Drawing.Color.Red;
            this.lblAttention.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAttention.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttention.ForeColor = System.Drawing.Color.White;
            this.lblAttention.Location = new System.Drawing.Point(12, 9);
            this.lblAttention.Name = "lblAttention";
            this.lblAttention.Size = new System.Drawing.Size(241, 268);
            this.lblAttention.TabIndex = 14;
            this.lblAttention.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAttention.Visible = false;
            this.lblAttention.Click += new System.EventHandler(this.LblAttention_Click);
            // 
            // lblDragPics
            // 
            this.lblDragPics.BackColor = System.Drawing.SystemColors.Control;
            this.lblDragPics.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDragPics.ForeColor = System.Drawing.Color.Black;
            this.lblDragPics.Location = new System.Drawing.Point(259, 66);
            this.lblDragPics.Name = "lblDragPics";
            this.lblDragPics.Size = new System.Drawing.Size(27, 70);
            this.lblDragPics.TabIndex = 21;
            this.lblDragPics.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPasswort
            // 
            this.btnPasswort.Location = new System.Drawing.Point(173, 227);
            this.btnPasswort.Name = "btnPasswort";
            this.btnPasswort.Size = new System.Drawing.Size(43, 20);
            this.btnPasswort.TabIndex = 24;
            this.btnPasswort.Text = "OK";
            this.btnPasswort.UseVisualStyleBackColor = true;
            this.btnPasswort.Visible = false;
            this.btnPasswort.Click += new System.EventHandler(this.BtnPasswort_Click);
            // 
            // tbxPasswort
            // 
            this.tbxPasswort.Location = new System.Drawing.Point(46, 227);
            this.tbxPasswort.Name = "tbxPasswort";
            this.tbxPasswort.PasswordChar = '*';
            this.tbxPasswort.Size = new System.Drawing.Size(121, 20);
            this.tbxPasswort.TabIndex = 23;
            this.tbxPasswort.Visible = false;
            this.tbxPasswort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TbxPasswort_KeyDown);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsFoot});
            this.statusStrip1.Location = new System.Drawing.Point(0, 287);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(267, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsFoot
            // 
            this.tsFoot.Image = global::schuelerfoto.Properties.Resources.pbxDbConnectFailed1;
            this.tsFoot.Name = "tsFoot";
            this.tsFoot.Size = new System.Drawing.Size(16, 17);
            // 
            // pbxInfo
            // 
            this.pbxInfo.Image = global::schuelerfoto.Properties.Resources.pbxInfo;
            this.pbxInfo.Location = new System.Drawing.Point(18, 19);
            this.pbxInfo.Name = "pbxInfo";
            this.pbxInfo.Size = new System.Drawing.Size(22, 22);
            this.pbxInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxInfo.TabIndex = 26;
            this.pbxInfo.TabStop = false;
            this.pbxInfo.Click += new System.EventHandler(this.PbxInfo_Click);
            // 
            // pbxRect
            // 
            this.pbxRect.Location = new System.Drawing.Point(12, 51);
            this.pbxRect.Name = "pbxRect";
            this.pbxRect.Size = new System.Drawing.Size(241, 236);
            this.pbxRect.TabIndex = 25;
            this.pbxRect.TabStop = false;
            this.pbxRect.Paint += new System.Windows.Forms.PaintEventHandler(this.PbxRect_Paint);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(267, 309);
            this.Controls.Add(this.pbxInfo);
            this.Controls.Add(this.pbxRect);
            this.Controls.Add(this.lblDragPics);
            this.Controls.Add(this.cbxKlasse);
            this.Controls.Add(this.btnPasswort);
            this.Controls.Add(this.tbxPasswort);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblAttention);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "SchülerFoto-NRW";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Form1_HelpButtonClicked);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.Form1_HelpRequested);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbxKlasse;
        private System.Windows.Forms.Label lblAttention;
        private System.Windows.Forms.Button btnPasswort;
        private System.Windows.Forms.TextBox tbxPasswort;
        private System.Windows.Forms.Label lblDragPics;
        private System.Windows.Forms.ToolStripStatusLabel tsFooter;
        private System.Windows.Forms.ToolStripStatusLabel tsFoot;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.PictureBox pbxRect;
        private System.Windows.Forms.PictureBox pbxInfo;
    }
}

