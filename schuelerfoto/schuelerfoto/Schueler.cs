﻿namespace schuelerfoto
{
    public class Schueler
    {
        public string Klasse { get; internal set; }
        public int ID { get; internal set; }
        public bool FotoVorhanden { get; internal set; }
        public bool FotoBinary { get; internal set; }
        public int SchulnrEigner { get; internal set; }
        public byte[] foto { get; internal set; }
    }
}