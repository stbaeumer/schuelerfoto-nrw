﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

// Published under the terms of GNU GENERAL PUBLIC LICENSE Version 3 
// © 2017 Stefan Bäumer

namespace schuelerfoto
{
    internal class DataAccess
    {
        private string connetionString;

        public DataAccess(string pw)
        {
            connetionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=DB\SchILD2000n.mdb; Persist Security Info = False;Jet OLEDB:Database Password = " + pw;
        }
                
        internal DateTime GetSchildVersion()
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connetionString))
            {
                try
                {
                    DateTime schildVersion = new DateTime();

                    string queryString = @"SELECT Schild_Verwaltung.Version FROM Schild_Verwaltung;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        schildVersion = Convert.ToDateTime((oleDbDataReader["Version"]).ToString());
                    }
                    oleDbDataReader.Close();
                    return schildVersion;
                }                
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Kein zulässiges Kennwort"))
                    {
                        throw new SchildException(
                            ex.Message + " Sie müssen das Kennwort eingeben, das herstellerseitig gesetzt wurde.",
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Datenbankkennwort eingeben!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            true,
                            "Das Datenbankkennwort ist nicht gesetzt oder falsch. " + ex.ToString()
                            );
                    }
                    if (ex.Message.Contains("Parameter"))
                    {
                        throw new SchildException(
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein: " + ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein. " + ex.ToString()
                            );
                    }
                    throw new SchildException(
                            ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            ex.ToString()
                            );
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }
        
        internal Klasses GetSchoolClasses()
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connetionString))
            {
                try
                {
                    Klasses klasses = new Klasses();            
                    int aktSj = DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.AddYears(-1).Year;

                    string queryString = @"SELECT Schueler.ID, Schueler.Klasse, Schueler.FotoVorhanden, Len([SchuelerFotos].[Foto]) AS Laenge, Schueler.SchulnrEigner 
FROM Schueler LEFT JOIN SchuelerFotos ON Schueler.ID = SchuelerFotos.Schueler_ID
WHERE (((Schueler.Geloescht)='-') AND ((Schueler.Status)=2) AND ((Schueler.AktSchuljahr)=" + aktSj + @"))ORDER BY Schueler.Klasse, Schueler.Name, Schueler.Vorname;";
                
                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        Schueler schueler = new Schueler()
                        {
                            ID = oleDbDataReader.GetInt32(0),
                            Klasse = SafeGetString(oleDbDataReader, 1),
                            FotoVorhanden = SafeGetString(oleDbDataReader, 2) == "-" ? false : true,
                            FotoBinary = oleDbDataReader.GetValue(3) == DBNull.Value ? false : true,
                            SchulnrEigner = oleDbDataReader.GetInt32(4)
                        };

                        Klasse klasse = (from kl in klasses where kl.Name == schueler.Klasse select kl).FirstOrDefault();

                        if (schueler.FotoBinary && !schueler.FotoVorhanden)
                        {
                            oleDbCommand = new OleDbCommand();
                            oleDbCommand = oleDbConnection.CreateCommand();
                            oleDbCommand.CommandText = "UPDATE Schueler SET FotoVorhanden = '+' WHERE ID = @id";
                            oleDbCommand.Parameters.AddWithValue("@id", schueler.ID);
                            oleDbCommand.ExecuteNonQuery();
                            schueler.FotoVorhanden = true;
                        }
                        if (!schueler.FotoBinary && schueler.FotoVorhanden)
                        {
                            oleDbCommand = new OleDbCommand();
                            oleDbCommand = oleDbConnection.CreateCommand();
                            oleDbCommand.CommandText = "UPDATE Schueler SET FotoVorhanden = '-' WHERE ID = @id";
                            oleDbCommand.Parameters.AddWithValue("@id", schueler.ID);
                            oleDbCommand.ExecuteNonQuery();
                            schueler.FotoVorhanden = true;
                        }

                        if (klasse != null)
                        {
                            klasse.Schuelers.Add(schueler);
                        }
                        else
                        {
                            klasse = new Klasse()
                            {
                                Name = schueler.Klasse
                            };
                            if (klasse.Name != "")
                            {
                                klasse.Schuelers = new Schuelers
                                {
                                    schueler
                                };
                                klasses.Add(klasse);
                            }
                        }
                    }
                    oleDbDataReader.Close();
                    return klasses;
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Parameter"))
                    {
                        throw new SchildException(
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein: " + ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein. " + ex.ToString()
                            );
                    }
                    throw new SchildException(
                            ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            ex.ToString()
                            );
                }
                finally
                {
                    oleDbConnection.Close();
                }                
            }
        }

        public string SafeGetString(OleDbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        internal void InsertImage(Schueler schueler)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connetionString))
            {
                try
                {
                    oleDbConnection.Open();
                    OleDbCommand oleDbCommand = new OleDbCommand();
                    oleDbCommand = oleDbConnection.CreateCommand();
                    oleDbCommand.CommandText = "UPDATE Schueler SET FotoVorhanden = '+' WHERE ID = @id";
                    oleDbCommand.Parameters.AddWithValue("@id", schueler.ID);
                    oleDbCommand.ExecuteNonQuery();
                    schueler.FotoVorhanden = true;
                    oleDbCommand = new OleDbCommand();
                    oleDbCommand = oleDbConnection.CreateCommand();
                    oleDbCommand.CommandText = "insert into SchuelerFotos (Schueler_ID, Foto, SchulnrEigner) values (@id, @foto, @schulnrEigner)";
                    oleDbCommand.Parameters.AddWithValue("@id", schueler.ID);
                    oleDbCommand.Parameters.AddWithValue("@foto", schueler.foto);
                    oleDbCommand.Parameters.AddWithValue("@schulnrEigner", schueler.SchulnrEigner);
                    oleDbCommand.ExecuteNonQuery();
                    schueler.FotoBinary = true;
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Parameter"))
                    {
                        throw new SchildException(
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein: " + ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein. " + ex.ToString()
                            );
                    }
                    throw new SchildException(
                            ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            ex.ToString()
                            );
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        internal void DeleteImage(Schueler schueler)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connetionString))
            {
                try
                {
                    oleDbConnection.Open();
                    OleDbCommand oleDbCommand = new OleDbCommand();
                    oleDbCommand = oleDbConnection.CreateCommand();
                    oleDbCommand.CommandText = "DELETE * FROM SchuelerFotos WHERE SchuelerFotos.Schueler_ID = @id";
                    oleDbCommand.Parameters.AddWithValue("@id", schueler.ID);
                    oleDbCommand.ExecuteNonQuery();
                    oleDbCommand = new OleDbCommand();
                    oleDbCommand = oleDbConnection.CreateCommand();
                    oleDbCommand.CommandText = "UPDATE Schueler SET FotoVorhanden = '-' WHERE ID = @id";
                    oleDbCommand.Parameters.AddWithValue("@id", schueler.ID);
                    oleDbCommand.ExecuteNonQuery();
                    schueler.FotoVorhanden = false;
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Parameter"))
                    {
                        throw new SchildException(
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein: " + ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            "Fehler beim Lesen der Datenbank. Dieser Fehler könnte durch eine syntaktisch falsche Anfrage an die Datenbank ausgelöst worden sein. " + ex.ToString()
                            );
                    }
                    throw new SchildException(
                            ex.Message,
                            new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                            System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                            true,
                            "Fehler!",
                            global::schuelerfoto.Properties.Resources.pbxInfo,
                            false,
                            ex.ToString()
                            );
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }
    }
}