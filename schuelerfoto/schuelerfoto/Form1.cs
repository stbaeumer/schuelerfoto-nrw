﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Published under the terms of GNU GENERAL PUBLIC LICENSE Version 3 
// © 2017 Stefan Bäumer

namespace schuelerfoto
{
    public partial class Form1 : Form
    {
        Klasses klasses = new Klasses();
        DataAccess dataAccess;
        private BackgroundWorker backgroundWorker = new BackgroundWorker();
        private string[] pics;
        private int cbxIndex = 0;        
        private DateTime schildVersionActual;
        public IEnumerable<DateTime> SchildVersionExpected { get; set; }
        public string TsFootTextVorher { get; set; }
        public Image TsFootImageVorher { get; set; }
        public string RectText { get; private set; }
        public Color RectBorderColor { get; private set; }
        public Brush RectBackColor { get; private set; }
        public Brush RectTextColor { get; private set; }

        public Form1()
        {
            InitializeComponent();                                   
            backgroundWorker.DoWork += new DoWorkEventHandler(BackgroundWorker_DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundWorker_RunWorkerCompleted);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(BackgroundWorker_ProgressChanged);
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;            
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            DateTime[] releases = {
                new DateTime(2017, 2, 11),
                new DateTime(2017, 5, 11),
                new DateTime(2017, 11, 14),
                new DateTime(2017, 10, 14),
                new DateTime(2018, 02, 02),
                new DateTime(2018, 06, 10)
            };

            SchildVersionExpected = releases;
            
            var toolTipDokumentation = new ToolTip();
            toolTipDokumentation.ToolTipTitle ="Dokumentation";
            btnPasswort.Visible = false;
            tbxPasswort.Visible = false;

            toolTipDokumentation.UseFading = true;
            toolTipDokumentation.UseAnimation = true;
            toolTipDokumentation.IsBalloon = true;
                        
            toolTipDokumentation.AutoPopDelay = 5000;
            toolTipDokumentation.InitialDelay = 1000;
            toolTipDokumentation.ReshowDelay = 500;            
            toolTipDokumentation.ShowAlways = true;            
            toolTipDokumentation.SetToolTip(this.pbxInfo, "Klicken Sie hier, um die Dokumentation online einzusehen.");
            
            RectBorderColor = System.Drawing.ColorTranslator.FromHtml("#3b88c3");
            RectBackColor = new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3"));
            RectTextColor = new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3"));

            try
            {
                #if DEBUG
                   Application.Exit();
                #endif

                dataAccess = new DataAccess(tbxPasswort.Text);
                
                if (!(new DirectoryInfo(Directory.GetCurrentDirectory().ToLower()).Name).Contains("schild-nrw"))
                    throw new SchildException(
                        "Sie müssen dieses Programm in das existierende SchILD-NRW Verzeichnis legen und dort doppelklicken!\n\r\n\r...\\SchILD-NRW\\SchuelerFoto-NRW.exe",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                        System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                        true,
                       "Programm nach SCHILD-NRW verschieben!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        ""
                        );
                
                if (!(File.Exists(Directory.GetCurrentDirectory() + "\\DB\\SchILD2000n.mdb")))
                    throw new SchildException(
                        "Die Datenbank wird nicht gefunden. \r\n\r\nSie muss im Ordner namens DB unterhalb von SchILD-NRW liegen!\r\n\r\n...\\SchILD-NRW\\DB\\SchILD2000n.mdb",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                        System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                        true,
                       "Programm nach SCHILD-NRW verschieben!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        ""
                        );
                
                if (!Properties.Settings.Default.licAccepted)
                    throw new SchildException(
                        "Dieses Programm steht Ihnen dauerhaft kostenlos unter den Bedingungen der \r\n\r\nGNU General Public License 3 (https://www.gnu.org/copyleft/gpl.html) \r\n\r\nzur Verfügung. \r\nKlicken Sie hier, um die Lizenzbedingungen zu akzeptieren.",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                        System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                        true,
                        "Lizenzbedingungen akzeptieren!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        ""
                        );
                
                schildVersionActual = dataAccess.GetSchildVersion();

                klasses = dataAccess.GetSchoolClasses();

                if (klasses.Count == 0)
                    throw new SchildException(
                        "Es scheint keinen einzigen aktiven Schüler im aktuellen Schuljahr zu geben, der einer Klasse zugeordnet ist. \r\n\r\nStimmt das Schuljahr? Sind evtl. keine Klassen angelegt?",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                        System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                        true,
                       "Schuljahr / Klassen prüfen!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        ""
                        );
                
                cbxKlasse.DataSource = (from k in klasses select "     " + k.Name.PadRight(6) + "(" + k.Schuelers.Count().ToString().PadLeft(2) + " SuS)").ToList();
                cbxKlasse.BackColor = System.Drawing.ColorTranslator.FromHtml("#3b88c3");
                
                if (SchildVersionExpected.All(x => x.Date > schildVersionActual.Date))                    
                {
                    throw new SchildException(
                        "Diese Version von SchuelerFoto-NRW ist aktuell nur freigegeben für die SchILD-Version ab dem " + SchildVersionExpected.Min().Date.ToShortDateString() + ".\n\rIhre SchILD-Version: " + schildVersionActual.Date.ToShortDateString() + ".",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                        System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                        true,
                        "SchILD updaten!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        "Diese Version von SchuelerFoto-NRW ist aktuell nur freigegeben für die SchILD-Version ab dem " + SchildVersionExpected.Min().Date.ToShortDateString() + ".\n\rIhre SchILD-Version: " + schildVersionActual.Date.ToShortDateString() + "."
                        );
                }

                if (SchildVersionExpected.All(x => x.Date < schildVersionActual.Date))
                {
                    try
                    {
                        System.Diagnostics.Process.Start("https://bitbucket.org/stbaeumer/schuelerfoto-nrw/downloads/");
                    }
                    catch (Exception)
                    {                        
                    }
                    
                    throw new SchildException(
                        "Diese Version von SchuelerFoto-NRW ist aktuell nur freigegeben für die SchILD-Version vom " + SchildVersionExpected.Max().Date.ToShortDateString() + ".\n\rIhre SchILD-Version: " + schildVersionActual.Date.ToShortDateString() + ". Schauen Sie hier: https://bitbucket.org/stbaeumer/schuelerfoto-nrw/downloads/",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                        System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                        true,
                        "Neue Version dieses Programms laden!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        "Diese Version von SchuelerFoto-NRW ist aktuell nur freigegeben für die SchILD-Version vom " + SchildVersionExpected.Max().Date.ToShortDateString() + ".\n\rIhre SchILD-Version: " + schildVersionActual.Date.ToShortDateString() + "."
                    );
                }

                this.DragEnter += new DragEventHandler(Form1_DragEnter);
                this.DragDrop += new DragEventHandler(Form1_DragDrop);
                this.DragLeave += new EventHandler(this.Form1_DragLeave);
                          
                if(klasses.Count() > 0)                
                    CbxKlasse_SelectedIndexChanged(null, null);
                
                lblAttention.SendToBack();
                lblAttention.Visible = false;

                tsFoot.Text = "Datenbankverbindung ok.";
                tsFoot.Image = global::schuelerfoto.Properties.Resources.pbxDbConnectOk;
                
                if (!(File.Exists(Directory.GetCurrentDirectory() + "\\SchuelerFoto-NRW.log")))
                {
                    WriteToLog("SchuelerFoto-NRW Log erstellt");
                    WriteToLog("=============================");
                }
            }            
            catch (SchildException ex)
            {
                lblAttention.Text = ex.RectText;
                RectBackColor = ex.RectTextColor;
                lblAttention.BackColor = ex.RectBorderColor;
                lblAttention.Visible = ex.LblAttentionVisible;
                tsFoot.Text = ex.TsFooterText;
                tsFoot.Image = ex.TsFooterImage;
                lblAttention.BringToFront();
                WriteToLog(ex.LogEntry);
                tbxPasswort.Visible = ex.PasswordVisible;
                btnPasswort.Visible = ex.PasswordVisible;

                if (ex.PasswordVisible)
                {                    
                    tbxPasswort.BringToFront();
                    btnPasswort.BringToFront();
                }
                else
                {                 
                    tbxPasswort.SendToBack();
                    btnPasswort.SendToBack();
                }
            }                        
            catch (Exception ex)
            {
                lblAttention.Visible = true;
                lblAttention.BringToFront();
                lblAttention.BackColor = System.Drawing.ColorTranslator.FromHtml("#3b88c3");
                lblAttention.Text = ex.Message;
                WriteToLog(ex.ToString());                
            }
            finally
            {    
                FormBorderStyle = FormBorderStyle.FixedDialog;
                MaximizeBox = false;
                MinimizeBox = false;
                pbxRect.Invalidate();                
            }
        }

        private void OpenLog()
        {
            var log = new ProcessStartInfo(Directory.GetCurrentDirectory() + "\\SchuelerFoto-NRW.log");
            Process.Start(log);            
        }

        private void WriteToLog(string log)
        {
            using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\SchuelerFoto-NRW.log"))
            {
                if (log != "")
                {
                    sw.WriteLine(String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now) + " : " + log);
                }                
            }
        }

        protected void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {            
            try
            {
                BackgroundWorker sendingWorker = (BackgroundWorker)sender;
                StringBuilder stringBuilder = new StringBuilder();
                var studentsOfClass = klasses[cbxIndex].Schuelers;
                
                for (int i = 0; i < pics.Length; i++)
                {
                    if (!sendingWorker.CancellationPending)
                    {
                        using (var image = Image.FromFile(pics[i]))
                        {
                            var x = Math.Max(image.Width / 2 - (Math.Min(image.Height, image.Width) / 2), 0);
                            var y = Math.Max(image.Height / 2 - (Math.Min(image.Height, image.Width) / 2), 0);
                            var w = Math.Min(image.Width, image.Height);
                            var h = w;
                            var image2 = CropImage(image, new Rectangle(x, y, w, h));
                            var image3 = ResizeImage(160, 160, image2);
                            var ms = new MemoryStream();
                            image3.Save(ms, ImageFormat.Jpeg);                            
                            var fotoByteArray = new byte[ms.Length];
                            ms.Position = 0;
                            ms.Read(fotoByteArray, 0, fotoByteArray.Length);
                            studentsOfClass[i].foto = fotoByteArray;
                            stringBuilder.Append(string.Format("Verarbeitung gestartet: {0}{1}", InsertImage(studentsOfClass[i]), Environment.NewLine));
                            sendingWorker.ReportProgress(i);
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        break;
                    }
                }
                e.Result = stringBuilder.ToString();
            }
            catch (OutOfMemoryException)
            {
                throw new SchildException(
                    "Mindestens ein Bild scheint beschädigt zu sein. Trotz zulässiger Dateiendung lässt es sich nicht öffnen. Vermutlich lassen sich die Bilder auch mit einem Bildbetrachter in Windows nicht öffnen. Prüfen Sie das und versuchen Sie es dann erneut!",
                    new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                    System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                    true,
                    "Bilder überprüfen!",
                    global::schuelerfoto.Properties.Resources.pbxInfo,
                    false,
                    "Mindestens ein Bild scheint beschädigt zu sein. Trotz zulässiger Dateiendung lässt es sich nicht öffnen. Vermutlich lassen sich die Bilder auch mit einem Bildbetrachter in Windows nicht öffnen. Prüfen Sie das und versuchen Sie es dann erneut!"
                    );
            }            
        }
        
        public Image ResizeImage(int newWidth, int newHeight, Image image)
        {
            try
            {
                int sourceWidth = image.Width;
                int sourceHeight = image.Height;

                if (sourceWidth < sourceHeight)
                {
                    int buff = newWidth;

                    newWidth = newHeight;
                    newHeight = buff;
                }

                int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
                float nPercent = 0, nPercentW = 0, nPercentH = 0;

                nPercentW = ((float)newWidth / (float)sourceWidth);
                nPercentH = ((float)newHeight / (float)sourceHeight);
                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                    destX = System.Convert.ToInt16((newWidth - (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = System.Convert.ToInt16((newHeight - (sourceHeight * nPercent)) / 2);
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                Bitmap bmPhoto = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

                bmPhoto.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.Clear(Color.Black);
                grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                grPhoto.DrawImage(image, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);                
                grPhoto.Dispose();
                image.Dispose();
                return bmPhoto;
            }
            catch (Exception ex)
            {
                throw new SchildException(
                    "Es ist ein Fehler beim Verkleinern eines Bildes aufgetreten: " + ex.Message,
                    new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3")),
                    System.Drawing.ColorTranslator.FromHtml("#3b88c3"),
                    true,
                    "Fehlermeldung lesen!",
                    global::schuelerfoto.Properties.Resources.pbxInfo,
                    false,
                    "Es ist ein Fehler beim Verkleinern eines Bildes aufgetreten: " + ex.ToString()
                    );
            }
        }

        private static Image CropImage(Image img, Rectangle cropArea)
        {
            using (Bitmap bmpImage = new Bitmap(img))
            {
                return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
            }
        }

        private int InsertImage(Schueler student)
        {
            dataAccess.DeleteImage(student);
            dataAccess.InsertImage(student);
            return 1;
        }

        protected void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (!e.Cancelled && e.Error == null)
                {
                    RectText = "Verarbeitung abgeschlossen.";
                    WriteToLog("Klasse " + klasses[cbxIndex].Name.PadRight(7) + " mit " + klasses[cbxIndex].Schuelers.Count.ToString().PadLeft(4) + " Schülerfotos erfolgreich nach SchILD übertragen.");
                    cbxKlasse.Enabled = true;
                }
                else
                {
                    throw e.Error;
                }
            }
            catch (SchildException ex)
            {
                lblAttention.Text = ex.RectText;
                RectBackColor = ex.RectTextColor;
                lblAttention.BackColor = ex.RectBorderColor;
                lblAttention.Visible = ex.LblAttentionVisible;
                tsFoot.Text = ex.TsFooterText;
                tsFoot.Image = ex.TsFooterImage;
                lblAttention.BringToFront();
                WriteToLog(ex.LogEntry);
            }

            pbxRect.Invalidate();         
        }
        
        protected void BackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {            
            RectText = string.Format("Verarbeitung: {0}/" + klasses[cbxIndex].Schuelers.Count + " ... ok." , e.ProgressPercentage + 1);
            pbxRect.Invalidate();
        }
        
        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (lblAttention.Visible || backgroundWorker.IsBusy)
            {
                return;
            }
            try
            {
                TsFootImageVorher = tsFoot.Image;
                TsFootTextVorher = tsFoot.Text;
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                    e.Effect = DragDropEffects.Copy;

                int countExpected = klasses[cbxIndex].Schuelers.Count;                
                int countActual = (from f in (string[])e.Data.GetData(DataFormats.FileDrop) where Path.GetExtension(f).ToLower() == ".jpg" || Path.GetExtension(f).ToLower() == ".jpeg" select f).Count();
                
                if (countExpected != countActual)
                {
                    // ... it might be a single directory, which is also ok.

                    int countActualDir = (from x in (string[])e.Data.GetData(DataFormats.FileDrop) select x).Count();
                    
                    var dir = ((from x in (string[]) e.Data.GetData(DataFormats.FileDrop) select x).FirstOrDefault());

                    if (Directory.Exists(dir))
                    {
                        if (countActualDir > 1)
                        {
                            throw new SchildException(
                                "Es ist durchaus erlaubt einen ganzen Ordner hierher zu ziehen. Sie versuchen jedoch mehr als einen einzelnen Ordner hierher zu ziehen.",
                                new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#be1931")),
                                System.Drawing.ColorTranslator.FromHtml("#be1931"),
                                true,
                                "Nur einen Ordner hierher ziehen!",
                                global::schuelerfoto.Properties.Resources.pbxInfo,
                                false,
                                ""
                            );
                        }

                        // Check files in folder

                        DirectoryInfo directory = new DirectoryInfo(dir);

                        int countActualJpegInFolder = (from f in directory.GetFiles() where f.Extension.ToLower() == ".jpg" || f.Extension.ToLower() == ".jpeg" select f).Count();

                        if (countActualJpegInFolder != countExpected)
                        {
                            throw new SchildException(
                                "Es ist erlaubt einen ganzen Ordner hierher zu ziehen. Der Ordner muss aber " + countExpected + " JPG-/JPEG-Dateien enthalten.",
                                new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#be1931")),
                                System.Drawing.ColorTranslator.FromHtml("#be1931"),
                                true,
                                "Anzahl der Bilddateien im Ordner prüfen!",
                                global::schuelerfoto.Properties.Resources.pbxInfo,
                                false,
                                ""
                            );
                        }

                        pics = (from f in directory.GetFiles() where f.Extension.ToLower() == ".jpg" || f.Extension.ToLower() == ".jpeg" select f.FullName).ToArray();
                    }
                    else
                    {
                        if (countActual != countExpected)
                            throw new SchildException(
                                "Sie versuchen " + countActual + " statt " + countExpected + " JPG-/JPEG-Datei" + (countExpected > 1 ? "en" : "") + " hierher zu ziehen.",
                                new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#be1931")),
                                System.Drawing.ColorTranslator.FromHtml("#be1931"),
                                true,
                                "Anzahl und Reihenfolge beachten!",
                                global::schuelerfoto.Properties.Resources.pbxInfo,
                                false,
                                ""
                            );
                    }
                }
                else
                {
                    pics = (from f in (string[])e.Data.GetData(DataFormats.FileDrop) where Path.GetExtension(f).ToLower() == ".jpg" || Path.GetExtension(f).ToLower() == ".jpeg" select f).ToArray();
                }
                
                RectBorderColor = Color.Green;
                RectTextColor = Brushes.Green;
                RectText = "Los geht's!\nHier fallen lassen!";
            }
            catch (SchildException ex)
            {
                RectText = ex.RectText;
                RectBorderColor = ex.RectBorderColor;
                RectTextColor = ex.RectTextColor;
                tsFoot.Image = ex.TsFooterImage;
                tsFoot.Text = ex.TsFooterText;
                WriteToLog(ex.LogEntry);
            }
            catch (Exception exception)
            {
                WriteToLog(exception.ToString());
                OpenLog();
            }
            finally
            {
                pbxRect.Invalidate();
            }
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            if (lblAttention.Visible || backgroundWorker.IsBusy || RectBorderColor != Color.Green)
            {   
                Form1_DragLeave(this, null);
                return;
            }
            try
            {
                cbxKlasse.Enabled = false;
                RectText = "Bitte warten ...";
                pbxRect.Invalidate();                
                Array.Sort(pics, StringComparer.InvariantCulture);
                int numericValue = 1;
                object[] arrObjects = new object[] { numericValue };
                if (!backgroundWorker.IsBusy)
                {
                    backgroundWorker.RunWorkerAsync(arrObjects);
                }                           
            }
            catch (Exception ex)
            {
                throw new SchildException(
                    "Es ist zu einem Fehler gekommen: " + ex.Message,
                    new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#be1931")),
                    System.Drawing.ColorTranslator.FromHtml("#be1931"),
                    true,
                    "Vorgeschriebene Dateiformate beachten!",
                    global::schuelerfoto.Properties.Resources.pbxInfo,
                    false,
                    "Es ist zu einem Fehler gekommen: " + ex.ToString()
                    );
            }           
        }

        private void Form1_DragLeave(object sender, EventArgs e)
        {
            if (lblAttention.Visible || backgroundWorker.IsBusy)
            {
                return;
            }
            RectBorderColor = System.Drawing.ColorTranslator.FromHtml("#3b88c3");
            RectTextColor = new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3"));            
            CbxKlasse_SelectedIndexChanged(null, null);            
            lblAttention.SendToBack();            
            tsFoot.Image = global::schuelerfoto.Properties.Resources.pbxDbConnectOk;            
            tsFoot.Text = "Datenbankverbindung ok.";
        }
        
        private void CbxKlasse_SelectedIndexChanged(object sender, EventArgs e)
        {            
            cbxIndex = cbxKlasse.SelectedIndex;            
            RectText = "Ziehen Sie (einen Ordner mit) " + klasses[cbxIndex].Schuelers.Count + " Fotos der Klasse " + klasses[cbxIndex].Name + " hierher.";
            RectBorderColor = System.Drawing.ColorTranslator.FromHtml("#3b88c3");
            RectTextColor = new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#3b88c3"));
            pbxRect.Invalidate();
        }
        
        private void BtnPasswort_Click(object sender, EventArgs e)
        {            
            Form1_Load(this, null);            
        }

        private void LblAttention_Click(object sender, EventArgs e)
        {
            if (!(new DirectoryInfo(Directory.GetCurrentDirectory().ToLower()).Name).Contains("schild-nrw"))
            {
                Application.Exit();
            }
            else if (!(File.Exists(Directory.GetCurrentDirectory() + "\\DB\\SchILD2000n.mdb")))
            {
                Application.Exit();
            }
            else if (!Properties.Settings.Default.licAccepted)
            {
                Properties.Settings.Default.licAccepted = true;
                Properties.Settings.Default.Save();
                try
                {
                    System.Diagnostics.Process.Start("https://www.gnu.org/copyleft/gpl.html");
                }
                catch (Exception ex)
                {
                    throw new SchildException(
                        "Der Browser zum Anzeigen der Lizenzbedingungen lässt sich nicht öffnen. Die Seite lautet: https://www.gnu.org/copyleft/gpl.html",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#be1931")),
                        System.Drawing.ColorTranslator.FromHtml("#be1931"),
                        true,
                        "Browser starten!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        "Der Browser zum Anzeigen der Lizenzbedingungen lässt sich nicht öffnen: " + ex.ToString()
                        );
                }
                
                Form1_Load(this, null);                
            }
            else if (!(from s in SchildVersionExpected where s.Date == schildVersionActual select s).Any())
            {
                Application.Exit();
            }
            else
            {
                Form1_Load(this, null);
            }   
        }
                
        private void PbxRect_Paint(object sender, PaintEventArgs e)
        {
            const float xradius = 20;
            const float yradius = 20;            
            const float margin = 10;
            float hgt = (pbxRect.ClientSize.Height - 3 * margin);
            RectangleF rect = new RectangleF(margin, margin, pbxRect.ClientSize.Width - 2 * margin, hgt);
            
            Font font1 = new Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Point);                
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            Pen pen = new Pen(RectBorderColor, 8)
            {
                DashStyle = System.Drawing.Drawing2D.DashStyle.Dash
            };
            GraphicsPath path = MakeRoundedRect(rect, xradius, yradius, true, true, true, true);
            e.Graphics.DrawString(RectText, font1, RectTextColor, rect, stringFormat);
            e.Graphics.DrawPath(pen, path);
        }
        
        private GraphicsPath MakeRoundedRect(RectangleF rect, float xradius, float yradius, bool round_ul, bool round_ur, bool round_lr, bool round_ll)
        {
            PointF point1, point2;
            GraphicsPath graphicsPath = new GraphicsPath();            
            if (round_ul)
            {
                RectangleF rectangleF = new RectangleF(rect.X, rect.Y, 2 * xradius, 2 * yradius);
                graphicsPath.AddArc(rectangleF, 180, 90);
                point1 = new PointF(rect.X + xradius, rect.Y);
            }
            else point1 = new PointF(rect.X, rect.Y);
            
            if (round_ur)
                point2 = new PointF(rect.Right - xradius, rect.Y);
            else
                point2 = new PointF(rect.Right, rect.Y);
            graphicsPath.AddLine(point1, point2);
            
            if (round_ur)
            {
                RectangleF rectangleF = new RectangleF(rect.Right - 2 * xradius, rect.Y, 2 * xradius, 2 * yradius);
                graphicsPath.AddArc(rectangleF, 270, 90);
                point1 = new PointF(rect.Right, rect.Y + yradius);
            }
            else point1 = new PointF(rect.Right, rect.Y);            
            if (round_lr)
                point2 = new PointF(rect.Right, rect.Bottom - yradius);
            else
                point2 = new PointF(rect.Right, rect.Bottom);
            graphicsPath.AddLine(point1, point2);
            
            if (round_lr)
            {
                RectangleF rectangleF = new RectangleF(rect.Right - 2 * xradius, rect.Bottom - 2 * yradius, 2 * xradius, 2 * yradius);
                graphicsPath.AddArc(rectangleF, 0, 90);
                point1 = new PointF(rect.Right - xradius, rect.Bottom);
            }
            else point1 = new PointF(rect.Right, rect.Bottom);
            
            if (round_ll)
                point2 = new PointF(rect.X + xradius, rect.Bottom);
            else
                point2 = new PointF(rect.X, rect.Bottom);
            graphicsPath.AddLine(point1, point2);
            
            if (round_ll)
            {
                RectangleF rectangleF = new RectangleF(rect.X, rect.Bottom - 2 * yradius, 2 * xradius, 2 * yradius);
                graphicsPath.AddArc(rectangleF, 90, 90);
                point1 = new PointF(rect.X, rect.Bottom - yradius);
            }
            else point1 = new PointF(rect.X, rect.Bottom);
            
            if (round_ul)
                point2 = new PointF(rect.X, rect.Y + yradius);
            else
                point2 = new PointF(rect.X, rect.Y);
            graphicsPath.AddLine(point1, point2);            
            graphicsPath.CloseFigure();
            return graphicsPath;
        }

        private void PbxInfo_Click(object sender, EventArgs e)
        {
            if (backgroundWorker.IsBusy)
                return;
            try
            {
                System.Diagnostics.Process.Start("https://bitbucket.org/stbaeumer/schuelerfoto-nrw/src");
                lblAttention.Text = "SchuelerFoto-NRW\n\nPublished under the terms of GNU Public License Version 3\n\n© " + DateTime.Now.Year + " Stefan Bäumer\n\nstefan.baeumer\n@berufskolleg-borken.de";
                lblAttention.BackColor = System.Drawing.ColorTranslator.FromHtml("#3b88c3");
                lblAttention.Visible = true;
                lblAttention.BringToFront();
                TsFootImageVorher = tsFoot.Image;
                TsFootTextVorher = tsFoot.Text;
                tsFoot.Text = "Klicken, um zurückzukehren!";
                tsFoot.Image = global::schuelerfoto.Properties.Resources.pbxInfo;
            }
            catch (Exception ex)
            {
                throw new SchildException(
                        "Der Browser zum Anzeigen der Dokumentation lässt sich nicht öffnen. Die Seite lautet: https://bitbucket.org/stbaeumer/schuelerfoto-nrw/src",
                        new SolidBrush(System.Drawing.ColorTranslator.FromHtml("#be1931")),
                        System.Drawing.ColorTranslator.FromHtml("#be1931"),
                        true,
                        "Browser starten!",
                        global::schuelerfoto.Properties.Resources.pbxInfo,
                        false,
                        "Der Browser zum Anzeigen der Dokumentation lässt sich nicht öffnen: " + ex.ToString()
                        );
            }            
        }

        private void TbxPasswort_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Form1_Load(this, null);
            }
        }

        private void cbxKlasse_Click(object sender, EventArgs e)
        {
            if (backgroundWorker.IsBusy)
                cbxKlasse.Enabled = false;
        }

        private void Form1_HelpButtonClicked(object sender, CancelEventArgs e)
        {
            PbxInfo_Click(this, e);
        }

        private void Form1_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            LblAttention_Click(this, null);            
        }        
    }
}